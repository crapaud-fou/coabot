
const bot = require('bbot');
const globe_delay = 60000; // 1 minute
var globe_said = {};

bot.global.text({
  contains: ['facebook', 'google', 'amazon', 'apple', 'microsoft']
}, (b) => {
  b.respondVia('react', ':hear_no_evil:');
}, {
  id: 'gafam-react'
});

bot.global.text(
  /pe?tit? globe/i
, (b) => {
  if (!globe_said[b.message.user.room] || new Date().getTime() > globe_said[b.message.user.room] + globe_delay) {
    b.respond('![pti globe](https://crapaud-fou.org/images/chat-ptiglobe.png)');
    globe_said[b.message.user.room] = new Date().getTime();
  }
}, {
  id: 'ptiglobe-direct'
})
