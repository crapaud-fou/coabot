Coabot - le gentil robot de la mare aux crapauds
==================================================

Coabot est basé sur [bbot](http://bbot.chat), un framework de bot pour rocketchat qui est encore en cours de développement. bbot est librement inspiré de hubot, le bot de chez github.

Voir https://wiki.crapaud-fou.org/coabot pour plus d'information à propos de ce bot.

Développement
------------

- `clone`
- `npm install`
- `edit .env`
- `npm start -- -m shell` (pour tester en local)
- `npm start`

Author
--------

- mose

License
---------

(c) copyright 2019 - mose - available under [MIT](LICENSE) License
